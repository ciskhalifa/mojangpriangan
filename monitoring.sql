-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2018 at 04:09 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monitoring`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_bot`
--

CREATE TABLE `m_bot` (
  `chat_id` int(25) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_bot`
--

INSERT INTO `m_bot` (`chat_id`, `username`, `first_name`, `last_name`) VALUES
(74727043, '', 'Teten', ''),
(332908516, 'dzikrihf', 'Dzikri Hikmat', 'Fauzi'),
(541347757, 'ciskhalifa', 'Christiantinus', 'Nesi');

-- --------------------------------------------------------

--
-- Table structure for table `m_client`
--

CREATE TABLE `m_client` (
  `kode` int(5) NOT NULL,
  `sid` text,
  `nama_client` varchar(50) DEFAULT NULL,
  `alamat` text,
  `ip` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `tgl_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_data` int(5) DEFAULT NULL,
  `tgl_modify` datetime DEFAULT NULL,
  `user_modify` int(5) DEFAULT NULL,
  `kode_perusahaan` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_client`
--

INSERT INTO `m_client` (`kode`, `sid`, `nama_client`, `alamat`, `ip`, `status`, `tgl_data`, `user_data`, `tgl_modify`, `user_modify`, `kode_perusahaan`) VALUES
(2, NULL, 'Christiantinus Nesi', 'Telkom Lembong', '10.11.3.133', 'Request timed out', '2018-02-15 16:45:55', 1, '2018-02-19 21:56:51', NULL, 2),
(3, '4706905-88215', 'VPNIP JAMSOSTEK Jl. R. Syamsudin ', 'GPON02-D3-SKB-4# 11/16:24 | FHTT0430af38', '192.168.19.0', 'Request timed out', '2018-02-14 20:41:48', NULL, '2018-02-19 22:03:46', 19, 3),
(4, '4706905-0020122551', 'JAMSOSTEK SID  Jl. Lodaya No. 42 S Bandung,42,Band', 'GPON03-D3-TRG-4# 15/2:14 | FHTT0615e5f0', '192.168.19.1', 'Request timed out', '2018-02-14 20:41:48', NULL, '2018-02-19 21:58:12', 19, 3),
(5, '4700400-0027577226', ' KEC.CARINGIN SUKABUMI', 'GPON06-D3-SKB-4# 17/10:12 | FHTT09904d58', '192.168.24.4', 'Request timed out', '2018-02-14 21:01:08', NULL, '2018-02-19 21:56:51', NULL, 4),
(6, '4700400-0027555191', 'DUKCAPIL KEC_BAROS_KAB_KOTA SUKABUMI', 'GPON03-D3-SKB-4_172.29.215.6', '192.168.24.63', 'Request timed out', '2018-02-14 21:01:08', NULL, '2018-02-19 21:56:51', NULL, 4),
(7, '4700400-0027602879', 'DUKCAPIL KEC_LEMBURSITU_KAB_KOTA SUKABUMI', 'GPON03-D3-SKB-4# 2/1:25 | FHTT042a8070', '192.168.24.64', 'Request timed out', '2018-02-14 21:01:08', NULL, '2018-02-19 22:03:46', NULL, 4),
(8, '4700400-0027577165', 'DUKCAPIL KEC_GUNUNGGURUH_KAB_SUKABUMI', 'GPON05-D3-SKB-4# 6/2:7 | FHTT09905cf8', '192.168.24.92', 'Destination host unreachable', '2018-02-14 21:01:08', NULL, '2018-02-19 22:03:46', NULL, 4),
(9, '4700400-0027577193', 'DUKCAPIL KEC_CICANTAYAN_KAB_SUKABUMI', 'GPON01-D3-SKB-4# 14/15:1|FHTT04298fb8', '192.168.24.93', 'Request timed out', '2018-02-14 21:01:08', NULL, '2018-02-19 21:56:51', NULL, 4),
(10, '4700400-0027577236', 'DUKCAPIL KEC_SUKARAJA_KAB_SUKABUMI', 'GPON01-D3-SKB-4# 2/13:4|FHTT112bd538', '192.168.24.96', 'Request timed out', '2018-02-14 21:01:08', NULL, '2018-02-19 22:01:24', NULL, 4),
(11, '4700400-0027577281', 'DUKCAPIL KEC_KEBONPEDES_KAB_SUKABUMI', 'GPON05-D3-SKB-4# 1/3:4 FHTT041ee0e0', '192.168.24.97', 'Destination host unreachable', '2018-02-14 21:01:08', NULL, '2018-02-19 22:01:24', NULL, 4),
(12, '4700400-0027577268', 'DUKCAPIL KEC_CIREUNGHAS_KAB_SUKABUMI', 'GPON06-D3-SKB-4# 14/13:6 | FHTT11281f80', '192.168.24.98', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(13, '4700400-0027577316', 'DUKCAPIL KEC_GEGER BITUNG_KAB_SUKABUMI', 'GPON06-D3-SKB-4# 13/15:4 | FHTT07070e80', '192.168.24.101', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(14, '4700400-0027577207', 'DUKCAPIL KEC_KADUDAMPIT_KAB_SUKABUMI', 'GPON06-D3-SKB-4# 6/3:6 | FHTT07063370', '192.168.24.132', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(15, '4700400-0027538115', 'DUKCAPIL KAB_KOT_SUKABUMI', 'GPON04-D3-SKB-4# 17/3:9 | FHTT0705d5c0', '192.168.24.133', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(16, '4700400-0027538052', 'DUKCAPIL KAB_KOT_KOTA SUKABUMI', 'GPON01-D3-SKB-4# 7/10:12 | FHTT042a8b40', '192.168.24.134', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(17, '4700400-0027577217', 'DUKCAPIL KEC_BOJONGSOANG_KAB_BANDUNG', 'GPON06-D3-CJA-4# 11/4:14 | FHTT07090df0', '192.168.24.150', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(18, '4700400-0027576904', 'DUKCAPIL KEC_MARGAHAYU_KAB_BANDUNG', 'GPON07-D3-BDK-4# 17/9:27 | FHTT041f2ab8', '192.168.24.151', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(19, '4700400-0027592865 ', 'DUKCAPIL KEC_BATUNUNGGAL_KAB_KOTA BANDUNG', 'AKSES_to_GPON02-D3-TRG-4_172.29.133.4', '192.168.24.220', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(20, '4700400-0027592851', 'DUKCAPIL KEC_LENGKONG_KAB_KOTA BANDUNG', 'AKSES_to_GPON03-D3-TRG-4_172.29.133.5', '192.168.24.221', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(21, '4700400-0027578506', 'DUKCAPIL KEC_BUAH BATU_KAB_KOTA BANDUNG', 'GPON06-D3-CJA-4# 4/10:1 | FHTT041f3a98', '192.168.24.230', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(22, '4700400-0027577761', 'DUKCAPIL KEC_RANCASARI_KAB_KOTA BANDUNG', 'GPON05-D3-CJA-4# 12/8:6 | FHTT042938d8  ', '192.168.24.231', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(23, '4700400-0027578516', 'DUKCAPIL KEC_GEDEBAGE_KAB_KOTA BANDUNG', 'GPON05-D3-CJA-4# 15/16:3 | FHTT041f56c8', '192.168.24.235', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(24, '4700400-0027603137', 'DUKCAPIL KEC_PANYILEUKAN_KAB_KOTA BANDUNG', 'GPON05-D3-UBR-4# 7/5:1 | FHTT041f2a28', '192.168.24.236', NULL, '2018-02-14 21:01:08', NULL, NULL, NULL, 4),
(25, '4700035-74957', 'VPNIP BNI Cabang Sukabumi ', 'GPON02-D3-SKB-4# 12/7:2| FHTT0429a578', '192.168.11.2', NULL, '2018-02-15 12:56:24', NULL, NULL, NULL, 5),
(26, '4700035-79274', 'VPNIP BNI Capem Ciwangi-Sukabumi ', 'GPON07-D3-SKB-4# 6/7:3|FHTT09923790', '192.168.11.3', NULL, '2018-02-15 12:56:24', NULL, NULL, NULL, 5),
(27, '4700035-94819', 'VPNIP BNI 46 INDOMARET LEMBUR SITU Jl. Pelabuhan I', 'GPON04-D3-SKB-4# 1/5: 1|FHTT07085068 (ont offline)', '192.168.11.4', NULL, '2018-02-15 12:56:24', NULL, NULL, NULL, 5),
(28, '4700035-32475', 'VPNIP BANK NEGARA INDONESIA', 'GPON02-D3-SKB-4# 5/11:14| FHTT042ee790', '192.168.11.5', NULL, '2018-02-15 12:56:24', NULL, NULL, NULL, 5),
(29, '4700035-74397', 'VPNIP BANK NEGARA INDONESIA 46TBK JL. RAYA BAROS', 'GPON03-D3-SKB-4# 8/7:10|FHTT0705dc30 (ONT Offline)', '192.168.11.6', NULL, '2018-02-15 12:56:24', NULL, NULL, NULL, 5),
(30, '4700035-0001562851', 'VPNIP BANK NEGARA INDONESIA 46Tbk(BNI 46)', 'GPON02-D3-SKB-4# 12/11:8|FHTT06193fc0 (ONT Offline)', '192.168.11.7', NULL, '2018-02-15 12:56:24', NULL, NULL, NULL, 5),
(31, '4700035-79224', 'BNI46 YDPP TELKOM CID 8770 FAI-4335', 'GPON05-D3-DGO-4# 3/8 : 7 |FHTT07094be8', '192.168.11.8', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:33:53', 19, 5),
(32, '4700035-69290', 'VPNIP BNI 46 BANDUNG', 'GPON02-D3-DGO-4# 7/4:8 | FHTT07098cb0', '192.168.11.9', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:37:34', 19, 5),
(33, '4700035-69287', 'VPNIP BNI 46 JL. CISITU BANDUNG', 'GPON02-D3-DGO-4# 8/11:4 | FHTT07085268', '192.168.11.10', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:37:55', 19, 5),
(34, ' 4700035-69294', 'VPNIP BNI JL. DIPATIUKUR,114-116', 'GPON02-D3-DGO-4# MRS', '192.168.11.12', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:39:50', 19, 5),
(35, '4700035-74088', 'VPNIP BANK NEGARA INDONESIA ', 'GPON04-D3-CJA-4# MRBC', '192.168.11.13', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:40:37', 19, 5),
(36, '4700035-99735', 'VPNIP BANK NEGARA INDONESIA 46TBK Jl. Cipadung B,1', 'GPON06-D3-UBR-4# MRBE', '192.168.11.15', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:41:00', 19, 5),
(37, '4700035-55001', 'VPNIP ATM BNI46 METRO SUPERINDO BANDUNG ', 'GPON04-D3-CJA-4# MRBC', '192.168.11.18', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:41:36', 19, 5),
(38, '4700035-09198', 'VPNIP BNI 46 JL. TERUSAN BUAH BATU', 'GPON04-D3-CJA-4# MRAU', '192.168.11.21', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:42:10', 19, 5),
(39, '4700035-00568', 'VPNIP BNI 46 JL. RANCABOLANG ', 'GPON05-D3-CJA-4# 14/4:21 | FHTT053c3300', '192.168.11.22', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:42:45', 19, 5),
(40, '4700035-51371', 'VPNIP ATM BNI 46, Rabbani, Dipatiukur', 'GPON02-D3-DGO-4# MRR', '192.168.11.25', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:43:33', 19, 5),
(41, '4700035-54007', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) STKS Dago Jl. ', 'GPON02-D3-DGO-4# 13/6:11 | FHTT0428bc90', '192.168.11.26', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:44:03', 19, 5),
(42, '4700035-67991', 'VPNIP BANK NEGARA INDONESIA (BNI46)KCP SYARIAH MIK', 'GPON04-D3-BDK-4#  1/10:8 | FHTT042df030', '192.168.11.31', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:44:33', 19, 5),
(43, '4700035-99733', 'VPNIP BNI 46 SOEKARNO HATTA 711A BANDUNG', 'GPON06-D3-TRG-4# MRAG', '192.168.11.38', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:45:30', 19, 5),
(44, '4700035-15178', 'VPNIP BNI 46 Jl.Raya Cinunuk Bandung', 'GPON06-D3-UBR-4# 4/12:7 | FHTT042a2a90', '192.168.11.47', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:46:04', 19, 5),
(45, '4700035-55438', 'VPNIP BNI ALFAMART DIPATIUKUR', 'GPON05-D3-DGO-4# 6/7:13 | FHTT06158e88', '192.168.11.72', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:47:11', 19, 5),
(46, '4700035-66011 ', 'BNI Jl. Raya Cibabat (BNI KCPS CIMAHI)', 'GPON03-D3-CMI-4#  3/7:18 | FHTT07094230', '192.168.11.81', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:47:37', 19, 5),
(47, '4700035-0031040144', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) CRISIS TELKOM1', 'GPON05-D3-SKB-4# 4/6 onu 6 >> FHTT09933de0', '192.168.11.88', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:48:14', 19, 5),
(48, '4700035-0020735341', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) Jl. Ir. Djuand', 'GPON02-D3-DGO-4# MRV', '192.168.11.91', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:49:06', 19, 5),
(49, '4700035-0021707457', 'BANK NEGARA INDONESIA 46TBK(BNI 46) JL. RA KOSASIH', 'GPON06-D3-SKB-4# 2/5 onu 11 >> FHTT0992ba38', '192.168.11.93', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:49:33', 19, 5),
(50, '4700035-0021768164', 'BANK NEGARA INDONESIA 46Tbk SID Jl. KOPO,599,BANDU', 'GPON04-D3-BDK-4# MRR', '192.168.11.113', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:49:51', 19, 5),
(51, '4700035-0031037758', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) JL PELAJAR PEJ', 'GPON02-D3-TRG-4# 7/11 : 8 | FHTT0614dea0', '192.168.11.126', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:50:12', 19, 5),
(52, '4700035-0031036406', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) JL RE MARTADIN', 'GPON02-D3-SKB-4# 11/10:24 | FHTT09933a08', '192.168.11.129', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:50:42', 19, 5),
(53, '4700035-0031036441', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) JL JEND SUDIRM', 'GPON02-D3-SKB-4# 3/12:30 | FHTT09933bc0', '192.168.11.130', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:51:09', 19, 5),
(54, '4700035-0031037759', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) JL PELAJAR PEJ', 'GPON02-D3-TRG-4#', '192.168.11.135', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:57:28', 19, 5),
(55, '4700035-0029515523', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) Jl. Kawaluyaan', 'GPON03-D3-TRG-4# 7/5 : 20 | FHTT06156c10', '192.168.11.144', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:52:38', 19, 5),
(56, '4700035-0027126148', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) RS DUSTIRA JL.', 'GPON04-D3-CMI-4# 4/10:5 | FHTT0427fdf0', '192.168.11.145', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:53:06', 19, 5),
(57, '4700035-0006332378', 'BNI46 KK Gatot Suebroto Bandung', 'GPON04-D3-TRG-4#  17/6 : 4 | FHTT098ea8e8', '192.168.11.180', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:57:33', 19, 5),
(58, '4700035-92950', 'BNI46 ITB BANDUNG', 'GPON02-D3-DGO-4# 5/15:6 | FHTT06142b90', '192.168.11.196', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:54:09', 19, 5),
(59, '4700035-0003731545', 'BNI 46 Ciwastra', 'GPON08-D3-CJA-4# 2/14:11 | FHTT11349b80', '192.168.11.199', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:56:23', 19, 5),
(60, '4700035-0029999801', 'BANK NEGARA INDONESIA 46Tbk(BNI 46) Jl. Raya Sukar', 'GPON04-D3-SKB-4# 4/12:22 | FHTT070854c0', '192.168.11.246', NULL, '2018-02-15 12:56:25', NULL, '2018-02-16 20:56:55', 19, 5),
(61, '4700033-34311', 'BCA JL. TIPAR GEDE 10-11 SUKABUMI', 'AKSES_to_GPON01-D3-SKB-4_172.29.215.4', '192.168.16.1', NULL, '2018-02-15 14:01:21', NULL, NULL, NULL, 6),
(62, '4701258-91658', 'BANK SYARIAH MANDIRI Jl. Komplek Metro Indah Mall ', 'GPON04-D3-CJA-4 # 172.29.219.4', '192.168.18.9', NULL, '2018-02-18 20:55:42', NULL, NULL, NULL, 7),
(63, '4701258-98119', 'BANK SYARIAH MANDIRI JL. RAYA CIBABAD NO.118', 'GPON03-D3-CMI-4# 3/8:31 | FHTT10243a18', '192.168.18.15', NULL, '2018-02-18 20:55:42', NULL, NULL, NULL, 7),
(64, '4701258-89077', 'BANK SYARIAH MANDIRI JL.BAROS,49', 'GPON04-D3-CMI-4# 6/2:8 | FHTT042a4ca8', '192.168.18.16', NULL, '2018-02-18 20:55:42', NULL, NULL, NULL, 7),
(65, '4701258-0006330578', 'BANK SYARIAH MANDIRI KPRK POS SUKABUMI', 'GPON07-D3-SKB-4# 4/9:12 | FHTT099291f8', '192.168.18.18', NULL, '2018-02-18 20:55:42', NULL, NULL, NULL, 7),
(66, '4701258-0031055846', 'BANK SYARIAH MANDIRI Jl. Jend. Sudirman Blok 112 R', 'GPON03-D3-SKB-4# 13/9:28 | FHTT0273e106', '192.168.18.19', NULL, '2018-02-18 20:55:42', NULL, NULL, NULL, 7),
(67, '4701258-0031038131', 'BANK SYARIAH MANDIRI Jl Soekarno Hatta Bandung 402', 'GPON02-D3-TRG-4# 172.29.133.4', '192.168.18.22', NULL, '2018-02-18 20:55:42', NULL, NULL, NULL, 7),
(68, '4701258-0004585770', 'BANK SYARIAH MANDIRI UMM', 'GPON02-D3-SKB-4# 12/10:21 | FHTT06194600', '192.168.18.25', NULL, '2018-02-18 20:55:42', NULL, NULL, NULL, 7),
(69, '4701258-0003726204', 'VPNIP BANK SYARIAH MANDIRI', 'GPON04-D3-SKB-4# 8/1:9 | FHTT061778d8', '192.168.18.28', NULL, '2018-02-18 20:55:42', NULL, NULL, NULL, 7),
(70, '4703048-0029646255', 'DISPENDA PROV JABAR (KCP BJB) JL.PELABUHAN 2 KM.7 ', 'GPON01-D3-SKB-4# 172.29.215.4', '192.168.20.7', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(71, '4703048-79589', 'DISPENDA PROV JABAR (BDG TENGAH ) JL.KAWALUYAAN', 'GPON03-D3-TRG-4# 172.29.133.5', '192.168.20.24', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(72, '4703048-61796', 'PEMPROV JABAR Jl. Soekarno-Hatta,629,Bandung', 'GPON02-D3-TRG-4# 16/3 onu 1 >> FHTT11257258', '192.168.20.36', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(73, '4703048-61562', 'PEMPROV JABAR Jl. Soekarno-Hatta,705,Bandung', 'GPON03-D3-TRG-4# 4/16 onu 14 >> FHTT06143d30', '192.168.20.44', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(74, '4703048-61575', 'PEMPROV JABAR Jl. Soekarno Hatta,576,Bandung', 'GPON06-D3-CJA-4# 172.29.219.6', '192.168.20.46', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(75, '4703048-0029689980', 'DISPENDA PROV JABAR KCP BJB TKI II Margaasih Blok ', 'GPON05-D3-BDK-4# 172.29.216.69', '192.168.20.47', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(76, '4703048-0031256669', 'PEMPROV JABAR Jl. Ir. H Juanda No. 250 Bandung,00,', 'GPON03-D3-DGO-4# 172.29.216.133', '192.168.20.91', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(77, '4703048-0031248292', 'PEMPROV JABAR Jl. Pacuan Kuda No.50 Arcamanik,00,B', 'GPON10-D3-UBR-4# 5/16 onu 6 >> FHTT11346fb8', '192.168.20.131', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(78, '4703048-0030786451', 'PEMPROV JABAR Jalan Soekarno Hatta KM. 15,No. 855,', 'GPON08-D3-UBR-4# 7/10 onu 28 >> FHTT0614b298', '192.168.20.145', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8),
(79, '4703048-0030331822', 'PEMPROV JABAR JALAN SOEKARNO-HATTA,528,BANDUNG', 'GPON06-D3-CJA-4# 172.29.219.6', '192.168.20.147', NULL, '2018-02-18 21:06:12', NULL, NULL, NULL, 8);

-- --------------------------------------------------------

--
-- Table structure for table `m_perusahaan`
--

CREATE TABLE `m_perusahaan` (
  `kode` int(5) NOT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `tgl_data` timestamp NULL DEFAULT NULL,
  `user_data` int(5) DEFAULT NULL,
  `tgl_modify` datetime DEFAULT NULL,
  `user_modify` int(5) DEFAULT NULL,
  `style` text,
  `chat_id` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_perusahaan`
--

INSERT INTO `m_perusahaan` (`kode`, `nama_perusahaan`, `tgl_data`, `user_data`, `tgl_modify`, `user_modify`, `style`, `chat_id`) VALUES
(2, 'Localhost', NULL, 1, '2018-02-15 17:46:09', 1, '#521919', '541347757'),
(3, 'Jamsostek', NULL, NULL, '2018-02-14 21:50:40', 19, '#5ddb0f', NULL),
(4, 'Dukcapil', NULL, NULL, '2018-02-14 22:01:29', 19, '#0dacde', NULL),
(5, 'BNI', NULL, NULL, '2018-02-15 13:56:46', 19, '#f2430c', NULL),
(6, 'BCA', NULL, NULL, '2018-02-15 15:02:01', 19, '#1706de', NULL),
(7, 'Mandiri Syariah', NULL, NULL, '2018-02-18 21:56:34', 19, '#10911d', NULL),
(8, 'Pemprov', NULL, NULL, '2018-02-18 22:06:31', 19, '#74de09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `kode` int(5) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` int(1) DEFAULT NULL,
  `tgl_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_data` int(5) DEFAULT NULL,
  `tgl_modify` datetime DEFAULT NULL,
  `user_modify` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`kode`, `nama`, `email`, `username`, `password`, `role`, `tgl_data`, `user_data`, `tgl_modify`, `user_modify`) VALUES
(1, 'christiantinus nesi', 'cis', 'cis', '0192023a7bbd73250516f069df18b500', 1, '2018-01-13 08:22:24', NULL, '2018-01-13 15:22:24', 1),
(2, 'admin', 'admin', 'admin', '0192023a7bbd73250516f069df18b500', 1, '2018-01-30 02:17:40', NULL, NULL, NULL),
(3, 'admin2', 'admin2', 'admin2', '0192023a7bbd73250516f069df18b500', 1, '2018-01-30 02:17:46', NULL, NULL, NULL),
(5, 'Christiantinus Nesi', 'christiantinusnesi@gmail.com', 'cis', '0192023a7bbd73250516f069df18b500', 1, '2018-01-29 06:51:39', NULL, NULL, NULL),
(6, 'Irsan Ramadhan', 'kidul,sanzjehova@gmail.com', '70348', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:47:29', 1, NULL, NULL),
(7, 'Ahmad Hanafiah', 'ahmadhanafiah1234@gmail.com', '55056', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:48:54', 1, NULL, NULL),
(8, 'Cresta Permana', 'permana.crez@gmail.com', '55058', '3dd512af0201261aa90ac644c24af5c6', 1, '2018-02-08 03:49:43', 1, '2018-02-15 17:42:52', 1),
(9, 'Devi Arum Sari', 'devi.arum89@gmail.com', '33936', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:50:12', 1, NULL, NULL),
(10, 'Ferdiansyah ', 'fhrdhy7@gmail.com', '55097', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:50:59', 1, NULL, NULL),
(11, 'Ferdy Cahyadi', 'indigotuner@gmail.com', '33891', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:51:32', 1, NULL, NULL),
(12, 'Fitri Handayani', 'v3zahra86@gmail.com', '33909', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:51:54', 1, NULL, NULL),
(13, 'Gemma Valdi Ramadhan', 'gemma.valdi8@gmail.com', '82471', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:52:28', 1, NULL, NULL),
(14, 'Happy Restu Nugraha', 'Happy.restu01@gmail.com', '33931', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:52:49', 1, NULL, NULL),
(15, 'Harya F Yana', 'aryafyana@gmail.com', '34045', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:53:17', 1, NULL, NULL),
(16, 'Sony Hernawan', 'ste0087@gmail.com', '33879', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:53:38', 1, NULL, NULL),
(17, 'Widya Adi Kristanto', 'widya.adik@gmail.com', '33934', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:53:57', 1, NULL, NULL),
(18, 'Wulandari Maulidya', 'wulandari.maulidya@gmail.com', '33893', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:54:17', 1, NULL, NULL),
(19, 'Yuni Kurniasari. S.Kom', 'YUNI.KURNIASARI@GMAIL.COM', '33919', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:54:39', 1, NULL, NULL),
(20, 'Muhammad Dimas Andry', 'dimasandry21@gmail.com', 'dimasandry21@gmail.com', '3dd512af0201261aa90ac644c24af5c6', 2, '2018-02-08 03:55:05', 1, NULL, NULL),
(21, 'Teten Yani Rohmat', '650653@telkom.co.id', '650653', '3dd512af0201261aa90ac644c24af5c6', 1, '2018-02-08 03:56:24', 1, NULL, NULL),
(22, 'Isti Nurul Shofyah', 'in.shofyah@gmail.com', '910174', '3dd512af0201261aa90ac644c24af5c6', 1, '2018-02-08 03:57:48', 1, NULL, NULL),
(23, NULL, NULL, NULL, NULL, NULL, '2018-02-15 16:39:08', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vclient`
-- (See below for the actual view)
--
CREATE TABLE `vclient` (
`kode` int(5)
,`ip` varchar(30)
,`nama_client` varchar(50)
,`alamat` text
,`status` varchar(50)
,`tgl_data` timestamp
,`user_data` int(5)
,`tgl_modify` datetime
,`user_modify` int(5)
,`kode_perusahaan` int(5)
,`nama_perusahaan` varchar(50)
,`ippelanggan` text
,`statuspelanggan` text
,`totalup` bigint(21)
,`totaldown` bigint(21)
,`countdata` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vdash`
-- (See below for the actual view)
--
CREATE TABLE `vdash` (
`kode` int(5)
,`ip` varchar(30)
,`nama_client` varchar(50)
,`alamat` text
,`status` varchar(50)
,`tgl_data` timestamp
,`user_data` int(5)
,`tgl_modify` datetime
,`user_modify` int(5)
,`kode_perusahaan` int(5)
,`nama_perusahaan` varchar(50)
,`style` text
,`countdata` bigint(21)
,`rto` bigint(21)
,`connect` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure for view `vclient`
--
DROP TABLE IF EXISTS `vclient`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vclient`  AS  (select `m_client`.`kode` AS `kode`,`m_client`.`ip` AS `ip`,`m_client`.`nama_client` AS `nama_client`,`m_client`.`alamat` AS `alamat`,`m_client`.`status` AS `status`,`m_client`.`tgl_data` AS `tgl_data`,`m_client`.`user_data` AS `user_data`,`m_client`.`tgl_modify` AS `tgl_modify`,`m_client`.`user_modify` AS `user_modify`,`m_client`.`kode_perusahaan` AS `kode_perusahaan`,`m_perusahaan`.`nama_perusahaan` AS `nama_perusahaan`,group_concat(`m_client`.`ip` separator ', ') AS `ippelanggan`,group_concat(`m_client`.`status` separator ', ') AS `statuspelanggan`,count(if((`m_client`.`status` = 'Connected'),1,NULL)) AS `totalup`,count(if((`m_client`.`status` = 'Request timed out'),1,NULL)) AS `totaldown`,count(0) AS `countdata` from (`m_client` left join `m_perusahaan` on((`m_client`.`kode_perusahaan` = `m_perusahaan`.`kode`))) group by `m_perusahaan`.`nama_perusahaan`) ;

-- --------------------------------------------------------

--
-- Structure for view `vdash`
--
DROP TABLE IF EXISTS `vdash`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vdash`  AS  (select `m_client`.`kode` AS `kode`,`m_client`.`ip` AS `ip`,`m_client`.`nama_client` AS `nama_client`,`m_client`.`alamat` AS `alamat`,`m_client`.`status` AS `status`,`m_client`.`tgl_data` AS `tgl_data`,`m_client`.`user_data` AS `user_data`,`m_client`.`tgl_modify` AS `tgl_modify`,`m_client`.`user_modify` AS `user_modify`,`m_client`.`kode_perusahaan` AS `kode_perusahaan`,`m_perusahaan`.`nama_perusahaan` AS `nama_perusahaan`,`m_perusahaan`.`style` AS `style`,count(0) AS `countdata`,count(if((`m_client`.`status` = 'Request timed out'),1,NULL)) AS `rto`,count(if((`m_client`.`status` = 'Connected'),1,NULL)) AS `connect` from (`m_client` left join `m_perusahaan` on((`m_client`.`kode_perusahaan` = `m_perusahaan`.`kode`))) where (1 = 1) group by `m_perusahaan`.`nama_perusahaan`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_bot`
--
ALTER TABLE `m_bot`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `m_client`
--
ALTER TABLE `m_client`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `m_perusahaan`
--
ALTER TABLE `m_perusahaan`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`kode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_client`
--
ALTER TABLE `m_client`
  MODIFY `kode` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `m_perusahaan`
--
ALTER TABLE `m_perusahaan`
  MODIFY `kode` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `kode` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
