<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= strtoupper($nama_perusahaan[0]->nama_perusahaan); ?></h3>
                </div>
                <input type="hidden" id="kode_perusahaan" value="<?= $kode_perusahaan; ?>">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="data-detail" class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <?php
                                    if ($kolom) {
                                        foreach ($kolom as $key => $value) {
                                            if (strlen($value) == 0) {
                                                echo '<th data-type="numeric"></th>';
                                            } else {
                                                echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                            }
                                        }
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($rowdata as $row): ?>
                                    <tr>
                                        <td><?= $row->no; ?></td>
                                        <td><?= $row->nama_client; ?></td>
                                        <td><?= $row->alamat; ?></td>
                                        <?php
                                        if ($row->status == "Request timed out" || $row->status == "Destination host unreachable" || $row->status == "Destination net unreachable" || $row->status == "NULL") {
                                            echo '<td><button type="button" class="btn btn-danger btn-circle"><i class="fa fa-power-off"></i></button></td>';
                                        } else {
                                            echo '<td><button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button></td>';
                                        }
                                        ?>
                                        <td><?= $row->tgl_data; ?></td>
                                        <td><?= $row->tgl_modify; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>