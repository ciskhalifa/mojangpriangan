<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

    public function __construct() {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index() {
        $data['content'] = 'dashboard';
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['kolom'] = array("No", "IP", "Nama Client", "Status", "Tanggal Update", "Opsi");
        $data['tabel'] = 'vclient';
        $data['jmlkolom'] = count($data['kolom']);
        $data['perusahaan'] = $this->Data_model->jalankanQuery("SELECT * FROM vclient", 3);
        $data['datagrafik'] = $this->Data_model->jalankanQuery("SELECT * FROM vdash", 3);
        $this->load->view('default', $data);
    }

    public function view_detail() {
        $data['content'] = 'view_detail';
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['nama_perusahaan'] = $this->Data_model->jalankanQuery("SELECT * FROM vdash WHERE kode_perusahaan='" . $this->uri->segment(3) . "' LIMIT 1", 3);
        $data['kolom'] = array("Kode Client", "Nama Client", "Alamat", "Status", "Tanggal Data", "Tanggal Update");
        $data['kode_perusahaan'] = $this->uri->segment(3);
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM (SELECT @row := @row + 1 AS no, m_client.* FROM m_client,(SELECT @row := 0) as r) as tab WHERE kode_perusahaan='" . $this->uri->segment(3) . "'", 3);
        // $this->getPing();
        $this->load->view('default', $data);
    }

    public function listData() {
        /*
         * list data 
         */
        if (IS_AJAX) {
            $aColumns = array("no", "ip", "nama_client", "alamat", "status", "tgl_data", "tgl_modify", "kode");
            $sIndexColumn = "kode";
            $sTable = 'vclient';
            $sTablex = '';
            $sWhere = " AND kode_perusahaan = '" . $this->uri->segment(4) . '"';
            $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere ";
            echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
        }
    }

    function getPing() {
        $q = $this->Data_model->selectData("m_client", "kode");
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date("Y/m/d H:i:s");
        
        for ($i = 0; $i < count($q); $i++) {
            $ip_client = $q[$i]->ip;
            $status = "";
            $kondisi = "kode";
            $kode = $q[$i]->kode;
            $oldstat = $q[$i]->status;
            exec("ping -n 1 $ip_client", $output[$ip_client], $status);
            $cut = explode(":", $output[$ip_client][2]);
            $rto = trim($cut[0], " .");
           
            switch ($rto) {
                case 'Request timed out':
                    $data['status'] = 'Request timed out';
                    $data['tgl_modify'] = $tgl;
                    $this->Data_model->updateDataWhere($data, 'm_client', array($kondisi => $kode));
                    // if ($oldstat == $rto) {
                        
                    // } else if($oldstat == "" || $oldstat != $rto){
                    //     $this->Data_model->updateDataWhere($data, 'm_client', array($kondisi => $kode));
                    //     //$this->kirimTele($kode);
                    // }
                    break;
                default:
                    $dhu = trim($cut[1], " .");
                    switch ($dhu) {
                        case 'Destination host unreachable':
                            $data['status'] = 'Destination host unreachable';
                            $data['tgl_modify'] = $tgl;
                            $this->Data_model->updateDataWhere($data, 'm_client', array($kondisi => $kode));

                            // if ($oldstat == $dhu) {
                                
                            // } else if($oldstat == "" || $oldstat != $dhu){
                            //     $this->Data_model->updateDataWhere($data, 'm_client', array($kondisi => $kode));
                            //    // $this->kirimTele($kode);
                            // }
                            break;
                        case 'Destination net unreachable':
                            $data['status'] = 'Destination host unreachable';
                            $data['tgl_modify'] = $tgl;
                            $this->Data_model->updateDataWhere($data, 'm_client', array($kondisi => $kode));

                            // if ($oldstat == $dhu) {
                                
                            // } else if($oldstat == "" ||$oldstat != $dhu){
                            //     $this->Data_model->updateDataWhere($data, 'm_client', array($kondisi => $kode));
                            //    // $this->kirimTele($kode);
                            // }

                            break;
                        default :
                            $data['status'] = 'Connected';
                            $data['tgl_modify'] = $tgl;
                            $this->Data_model->updateDataWhere($data, 'm_client', array($kondisi => $kode));

                            // if ($oldstat == $dhu) {
                                
                            // } else if($oldstat == ""  || $oldstat != $dhu){
                            //     $this->Data_model->updateDataWhere($data, 'm_client', array($kondisi => $kode));
                            //     // $this->kirimTele($kode);
                            // }

                            break;
                    }
            }
        }
//        $this->kirimTele($kode);
    }

    public function Broadcast($chatid, $text) {

        $token = "494639317:AAEbjwDNwAA6zyHk-Y4PuaTYzNT4lGaYNn8";
        if (is_array($chatid)) {
            foreach ($chatid as $row) {
                $url = "https://api.telegram.org/bot" . $token . "/";
                $url .= "sendMessage?chat_id=" . $row . "&text=" . urlencode($text);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, NULL);
                $output = curl_exec($ch);
            }
            curl_close($ch);
            echo "Berhasil Di kirim";
        } else {
            $url = "https://api.telegram.org/bot" . $token . "/";
            $url .= "sendMessage?chat_id=" . $chatid . "&text=" . urlencode($text);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, NULL);
            $output = curl_exec($ch);
            curl_close($ch);
            echo "Berhasil Di kirim";
        }
    }

    public function kirimTele($kode) {
        $datagrafik = $this->Data_model->jalankanQuery("SELECT m_client.*, m_perusahaan.nama_perusahaan, m_perusahaan.chat_id FROM m_client JOIN m_perusahaan ON m_perusahaan.kode = m_client.kode_perusahaan WHERE kode_perusahaan= " . $kode, 3);
        if (empty($datagrafik)) {
            
        } else {
            $chat = explode(",", $datagrafik[0]->chat_id);
        }
        foreach ($datagrafik as $key) {
            if ($key->status == "Connected") {
                $statusclient = "UP";
            } else {
                $statusclient = "DOWN";
            }
            $chat = explode(",", $key->chat_id);
            $text = "Informasi STATUS Jaringan\n";
            $text .= "Pelanggan : " . $key->nama_perusahaan . "\n";
            $text .= "SID : " . $key->sid . "\n";
            $text .= "Alamat : " . $key->alamat . "\n";
            $text .= "Status : " . $statusclient;
            $text .= "\n\n Terima Kasih";
            $this->Broadcast($chat, $text);
        }
    }

}
