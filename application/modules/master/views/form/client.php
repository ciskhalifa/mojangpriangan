<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai):
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = ($aep == 'salin') ? '' : $arey['kode'];
}else {
    $cid = '';
}
?>

<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group">
            <label class="col-md-2 label-control">Nama Client</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Nama Client" name="nama_client" id="nama_client" value="<?= (isset($arey)) ? $arey['nama_client'] : ''; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 label-control">SID</label>
            <div class="col-md-4">
                <textarea class="form-control" name="sid" id="sid"><?= (isset($arey)) ? $arey['sid'] : ''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 label-control">Alamat Client</label>
            <div class="col-md-4">
                <textarea class="form-control" name="alamat" id="alamat"><?= (isset($arey)) ? $arey['alamat'] : ''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 label-control">IP Client</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="IP Client" name="ip" id="ip" value="<?= (isset($arey)) ? $arey['ip'] : ''; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 label-control">ID Perusahaan</label>
            <div class="col-md-4">
                <select name="kode_perusahaan" class="select2 form-control">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($arey)) ? $arey['kode_perusahaan'] : '';
                    $q = $this->Data_model->selectData("m_perusahaan", 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected="selected"' : '';
                        echo '<option value = "' . $row->kode . '" ' . $kapilih . '>' . $row->nama_perusahaan . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>

<script>
    $(function () {
        $('.select2').select2();
        $("#tmblBatal").on("click", function () {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function (c) {
            if (c.isDefaultPrevented()) {
            } else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = $("#xfrm").serialize();
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    dataType: "html",
                    beforeSend: function () {
                        $(".box #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                        })
                    },
                    success: function (d) {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function () {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>