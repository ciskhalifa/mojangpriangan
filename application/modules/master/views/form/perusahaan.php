<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai):
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = ($aep == 'salin') ? '' : $arey['kode'];
}else {
    $cid = '';
}
?>
<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group">
            <label class="col-md-2 label-control">Nama Perusahaan</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Nama Perusahaan" name="nama_perusahaan" id="nama_perusahaan" value="<?= (isset($arey)) ? $arey['nama_perusahaan'] : ''; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 label-control">Warna Kartu</label>
            <div class="col-md-4">
                <input type="text" name="style" class="form-control my-colorpicker1" placeholder="Color" value="<?= (isset($arey)) ? $arey['style'] : ''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 label-control">Broadcast Telegram</label>
            <div class="col-md-4">
                <select name="chat_id[]" class="select2 form-control" multiple="multiple">
                    <?php
                    $n = explode(",", $arey['']);
                    $q = $this->Data_model->selectData("m_bot", 'chat_id');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected="selected"' : '';
                        echo '<option value = "' . $row->chat_id . '">' . $row->first_name .' '. $row->last_name . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<!-- bootstrap color picker -->
<script src="<?= base_url('assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js'); ?>"></script>
<script>
    $(function () {
        $('select').select2();
        $('.my-colorpicker1').colorpicker();
        $("#tmblBatal").on("click", function () {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function (c) {
            if (c.isDefaultPrevented()) {
            } else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = $("#xfrm").serialize();
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    dataType: "html",
                    beforeSend: function () {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                        })
                    },
                    success: function (d) {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function () {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>