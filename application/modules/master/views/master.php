<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Master Data</h3>
                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" id="import"><i class="fa fa-upload"></i></button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="menuitem" data-default="perusahaan"><a href="javascript:;"><i class="fa fa-building"></i> Perusahaan </a></li>
                        <li class="menuitem"data-default="client"><a href="javascript:;"><i class="fa fa-users"></i> Client</a></li>
                        <li class="menuitem" data-default="user"><a href="javascript:;"><i class="fa fa-users"></i> Users</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="divhalaman"></div>
    </div>

</section>

<div class="modal fade text-xs-left" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form  role="form" id="sendFormImport" enctype="multipart/form-data"  class="form-horizontal">
                <div class="modal-header">
                    <h4 class="modal-title">Import Data</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">Perusahaan</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="perusahaan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">File</label>
                        <div class="col-md-4">
                            <input type="file" name="file" id="file">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

