<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<div id="myConfirm" class="modal fade">
    <div class="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/bower_components/select2/dist/css/select2.min.css'); ?>">
<!-- Select2 -->
<script src="<?= base_url('assets/bower_components/select2/dist/js/select2.full.min.js'); ?>"></script>
<script src="<?= base_url('assets/jquery.isloading.min.js'); ?>"></script>
<script>
    var myApp = myApp || {};
    var p;
    $(function () {
        $('#import').on('click', function () {
            $('#modalImport').modal();
        });
        $('#sendFormImport').on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'master/importExcel/';
                var data = new FormData(this);
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $("body").isLoading({
                            text: "",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                        });
                    },
                    success: function (html) {
                        setTimeout(function () {
                            scrollTo();
                            $('#modalImport').modal('hide');
                            notify('Data berhasil disimpan!', 'success');
                        }, 500);
                        $("body").isLoading("hide");
                        $('#modalImport').modal("hide");
//                        location.reload(true);
                    },
                    error: function () {
                        notify('Data gagal disimpan!', 'warning');
                        $('#modalImport').modal('hide');
                        $("body").isLoading("hide");
                    },
                });
                return false;
            }
        });
        $(".menuitem").bind('click', function () {
            if (!$(this).hasClass('active')) {
                $('.menuitem').removeClass('active');
                $(this).addClass('active');
                $('.jdlmaster').text($(this).attr('data-default').replace("_", " "));
                $.ajax({
                    type: 'POST',
                    url: 'master/loadHalaman/' + $(this).attr('data-default'),
                    success: function (result) {
                        if (result.length == 0) {
                            myApp.oTable.fnDestroy();
                            $("#divhalaman").html('');
                        } else {
                            $("#divhalaman").html(result);
                        }
                        scrollTo();
                    }
                });
            }
        });
        $("#btnYes").bind("click", function () {
            var link = $("#getto").val();
            $.ajax({url: link, type: "POST", data: "cid=" + $("#cid").val() + "&cod=" + $("#tabel").val(), dataType: "html", beforeSend: function () {
                    if (link != "#") {
                    }
                }, success: function (html) {
                    myApp.oTable.fnDraw(false);
                    $("#myConfirm").modal("hide")
                }})
        });
    });
</script>


