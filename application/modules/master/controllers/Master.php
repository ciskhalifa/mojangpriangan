<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Master extends MX_Controller {

    public function __construct() {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index() {
        $data['content'] = 'master';
        $data['js'] = 'js';
        $data['css'] = 'css';

        $this->load->view('default', $data);
    }

    function loadHalaman() {
        $konten['tabel'] = $this->uri->segment(3);
        switch ($this->uri->segment(3)) :
            case 'client':
                $konten['kolom'] = array("Kode", "IP Client", "Perusahaan", "Nama Client", "Status", "Tanggal Data", "");
                break;
            case 'user':
                $konten['kolom'] = array("Kode", "Nama", "Email", "Username", "Role", "");
                break;
            case 'perusahaan':
                $konten['kolom'] = array("Kode", "Nama Perusahaan", "Warna", "");
                break;
            default :
                $konten['kolom'] = "";
                break;
        endswitch;

        $konten['jmlkolom'] = count($konten['kolom']);
        $this->load->view('halaman', $konten);
    }

    function getData() {
        /*
         * list data 
         */
        if (IS_AJAX) {
            $sTablex = "";
            $order = "";
            $sTable = 'm_' . $this->uri->segment(3);
            $k = '';
            switch ($this->uri->segment(3)) :
                case 'client':
                    $aColumns = array("kode", "ip", "nama_perusahaan", "nama_client", "status", "tgl_data", "opsi");
                    $sTable = "( SELECT $sTable.*, nama_perusahaan FROM $sTable  LEFT JOIN m_perusahaan ON m_perusahaan.kode = $sTable.kode_perusahaan )";
                    $kolom = "kode, ip, nama_perusahaan, nama_client, status, tgl_data";
                    $sIndexColumn = "kode";
                    break;
                case 'user':
                    $aColumns = array("kode", "nama", "email", "username", "role", "opsi");
                    $kolom = "kode, nama, email, username, IF(role=1, 'Administrator', 'User') as role";
                    $sIndexColumn = "kode";
                    break;
                case 'perusahaan':
                    $aColumns = array("kode", "nama_perusahaan", "style", "opsi");
                    $kolom = "kode, nama_perusahaan, style";
                    $sIndexColumn = "kode";
                    break;
                default :
                    $sTable = 'm_perusahaan';
                    $aColumns = array("kode", "nama_perusahaan", "opsi");
                    $kolom = "kode, nama_perusahaan";
                    $sIndexColumn = "kode";
                    break;
            endswitch;
            if (isset($kolom) && strlen($kolom) > 0) {
                $where = "";
                $tQuery = "SELECT $kolom ,'$k' AS opsi  "
                        . "FROM $sTable a $sTablex $where WHERE 1=1";
                echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
            } else {
                echo "";
            }
        }
    }

    function loadForm() {
        $konten['aep'] = $this->uri->segment(5);
        if ($this->uri->segment(4) <> '-') {
            $nilai = $this->uri->segment(4);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                if ($this->uri->segment(6) == '' || strlen(trim($this->uri->segment(6))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(6)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            } else {
                if ($this->uri->segment(5) == '' || strlen(trim($this->uri->segment(5))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(5)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            }
            $konten['rowdata'] = $this->Data_model->satuData($tabel, $kondisi);
        }
        $this->load->view('form/' . $this->uri->segment(3), $konten);
    }

    function simpanData() {
        $arrdata = array();
        $chatid = $this->input->post('chat_id');
        $cid = '';
        $tabel = 'm_' . $this->uri->segment(3);
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) {
                
            } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                if ($key == 'cid') {
                    $cid = $value;
                } else {
                    if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("/", $value);
                            $newtgl = $tgl[1] . "/" . $tgl[0] . "/" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = NULL;
                        }
                    } else {
                        if ($this->uri->segment(4) == "user") {
                            $arrdata['password'] = md5($this->input->post('password'));
                            $arrdata[$key] = $value;
                        }
                        $arrdata[$key] = $value;
                    }
                }
            }
        }
        if ($cid == "") {
            if (is_array($chatid) || isset($chatid)) {
                $arrdata['chat_id'] = implode(",", $chatid);
            }else{

            }
            $arrdata['user_data'] = $_SESSION['kode'];
            try {
                $this->Data_model->simpanData($arrdata, $tabel);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            if (is_array($chatid) || isset($chatid)) {
                $arrdata['chat_id'] = implode(",", $chatid);
            } else {
            	
            }
            $kondisi = 'kode';
            $arrdata['tgl_modify'] = date('Y-m-d H:i:s');
            $arrdata['user_modify'] = $_SESSION['kode'];
            try {
                $kondisi = array('kode' => $cid);
                echo $this->Data_model->updateDataWhere($arrdata, $tabel, $kondisi);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
    }

    function hapus() {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('m_' . $this->input->post('cod'), $kondisi);
                echo json_encode("ok");
            }
        }
    }

    public function importExcel() {
        $basepath = BASEPATH;
        date_default_timezone_set("Asia/Bangkok");
        $date = date("Y-m-d H:i:s");
        $stringreplace = str_replace("system", "publik/", $basepath);
        $config['upload_path'] = $stringreplace;
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $files = $_FILES;
        $_FILES['userfile']['name'] = $files['file']['name'];
        $_FILES['userfile']['type'] = $files['file']['type'];
        $_FILES['userfile']['tmp_name'] = $files['file']['tmp_name'];
        $_FILES['userfile']['error'] = $files['file']['error'];
        $_FILES['userfile']['size'] = $files['file']['size'];
        $new_name = $files['file']['name'];
        $config['file_name'] = $new_name;
        //print_r($config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload()) {
            $this->prosesXLS($new_name, $this->input->post('perusahaan'));
        } else {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        }
    }

    function prosesXLS($namafile, $perusahaan) {
        error_reporting(E_ALL & ~E_NOTICE);
        $urutankode = 1;
        $nama = str_replace(" ", "_", $namafile);
        $file = 'publik/' . $nama;
        /*
         * load the excel library
         */
        $this->load->library('excel');
        /*
         * read file from path
         */
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        /*
         * get only the Cell Collection
         */
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        /*
         * extract to a PHP readable array format
         */

        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            $data_type = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();

            /*
             * header will/should be in row 1 only. of course this can be modified to suit your need.
             */
            if ($row <= 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }

        $kolom = array("", "sid", "nama_client", "alamat", "ip", "");

        foreach ($arr_data as $row):
            if ($this->cekIsiData($row, $baris) == true) :
                $kode = intval($kl) . $urutankode;
                /*
                 * $x := nilai untuk array $kolom
                 * $k := nilai untuk array xls
                 */
                $x = 0;
                $k = 1;
                $i = 1;
                $data = array();

                foreach ($row as $col):
                    if (substr($col, 0, 1) == "=") {
                        $nilai = "";
                    } else {
                        $nilai = ($col == '') ? NULL : $col;
                    }
                    switch ($x):
                        case 0:
                        case 5:
                            break;
                        default :
                            $data[$kolom[$x]] = $nilai;
                            break;
                    endswitch;
                    $k++;
                    $x++;

                endforeach;
                $i++;
                $q = $this->Data_model->jalankanQuery("SELECT * FROM m_perusahaan WHERE nama_perusahaan LIKE '%$perusahaan%' LIMIT 1", 1);
                if (empty($q)) {
                    $this->Data_model->simpanData(array('nama_perusahaan' => $perusahaan), 'm_perusahaan');
                    $a = $this->Data_model->jalankanQuery("SELECT max(kode) as kode FROM m_perusahaan LIMIT 1", 1);
                    $data['kode_perusahaan'] = $a->kode;
                } else {
                    $data['kode_perusahaan'] = $q->kode;
                }
                try {
                    $this->Data_model->simpanData($data, 'm_client');
                } catch (Exception $exc) {
                    $data['infoerror'] = $exc->getMessage();
                }
            else:

            endif;

        endforeach;
    }

    function cekIsiData($row, $baris) {
        $isi = false;
        foreach ($row as $eusi) {
            $isi = (strlen(trim($eusi)) > 0) ? true : (($isi == true) ? $isi : false);
        }
        return $isi;
    }

}
