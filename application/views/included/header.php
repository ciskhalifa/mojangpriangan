<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="<?= APP_NAME; ?>">
        <meta name="keywords" content="HTML,CSS,JavaScript">
        <meta name="author" content="<?= AUTHOR; ?>">
        <title><?= APP_NAME; ?> | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?= base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url('assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url('assets/bower_components/Ionicons/css/ionicons.min.css') ?>">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?= base_url('assets/bower_components/jvectormap/jquery-jvectormap.css') ?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/AdminLTE.min.css') ?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/skins/_all-skins.min.css') ?>">
        <!-- Google Font -->
        
        <!-- CUSTOM CSS -->
        <?php ($css != '') ? $this->load->view($css) : ''; ?>
    </head>
    <?php if ($this->uri->segment(1) == "dashboard") { $onload = 'onload="JavaScript:AutoRefresh(60000)"';}else{$onload="";}?>
    <body class="hold-transition sidebar-mini skin-red-light sidebar-collapse" <?= $onload; ?>>
        <header class="main-header">
            <!-- Logo -->
            <a href="<?= base_url('dashboard'); ?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>M</b>JA</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>MO</b>JA</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= base_url('assets/dist/img/avatar5.png'); ?>" class="user-image" alt="<?= (isset($_SESSION['nama']) ? $_SESSION['nama'] : ''); ?>">
                                <span class="hidden-xs"><?= (isset($_SESSION['nama']) ? $_SESSION['nama'] : ''); ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= base_url('assets/dist/img/avatar5.png'); ?>" class="img-circle" alt="<?= (isset($_SESSION['nama']) ? $_SESSION['nama'] : ''); ?>">
                                    <p>
                                        <?= (isset($_SESSION['nama']) ? $_SESSION['nama'] : ''); ?> - Web Developer
                                        <small>Member since <?= $_SESSION['tgl_data']; ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?= base_url('login/doOut') ?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>